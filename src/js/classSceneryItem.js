"use strict";
// export
class SceneryItem {
  constructor(name, kind, description= null, clue = false){
    this.name = name;
    this.image = `url(./images/${this.name}.png)`;
    this.kind = kind;
    this.clue = clue;
    this.description = description;
  }

  getDescription(bgColor){
    for (let i = 0; i < this.description.length; i++) {
      let divDescription = document.createElement("div");
      if (this.clue) {
        divDescription.setAttribute(`class`, `${this.description[i]["name"]} col-md-2 clue`);
        sectionDescription.appendChild(divDescription);
      } else {
        divDescription.setAttribute(`class`, `${this.description[i]["name"]} col-md-2 ${bgColor}`);
        sectionDescription.appendChild(divDescription);
      }
    }
  }
}